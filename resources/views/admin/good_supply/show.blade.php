@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/good_supplies')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Good Supply Shown:</h4>
                    </div>
                </div>
                <br>
                @if($errors->any())
                    @foreach($errors as $error)
                        {{$error}}
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/good_supplies','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="Start_date" class="col-4 col-form-label">Date:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$good->date}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Project Name:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$good->project->name}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Goods:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$good->purchases->name}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Quantity:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$good->quantity}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-4 offset-4">
                                    <button type="button" class="btn btn-info waves-effect waves-light">
                                        <A href='{{url('myadmin/good_supplies')}}'>Back</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

    </div>
@endsection