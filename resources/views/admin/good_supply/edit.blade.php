@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/good_supplies')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Good Supply Edit Form:</h4>
                    </div>
                </div>
                <br>
                @if($errors->any())
                    @foreach($errors as $error)
                        {{$error}}
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/good_supplies/'.$good->id,'method'=>'PUT','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="Start_date" class="col-4 col-form-label">Date:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text" required="" name="date"class="form-control datepicker"  id="Start_date" placeholder="Enter date" value="{{old('date')?old('date'):$good->date}}">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Project Name:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="project_id" id="project_id" required="" class="form-control">
                                        <option selected>Select project</option>
                                        @foreach($projects as $project)
                                            <option  @if($good->project_id ==$project->id ) selected @endif value="{{$project->id}}">{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Goods:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="purchase_id" id="purchase_id" required="" class="form-control" autocomplete="on">
                                        <option selected>Select goods</option>
                                        @foreach($purchases as $purchase)
                                            <option  @if( $good->purchase_id == $purchase->id) selected @endif value="{{$purchase->id}}">{{$purchase->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Quantity:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="number" step="0.01" min="0" max="10000" required="" name="quantity" class="form-control" id="name" placeholder="Enter Name" value="{{old('quantity')?old('quantity'):$good->quantity}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-4 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        <A href='{{url('myadmin/good_supplies')}}'>Cancel</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

    </div>
@endsection