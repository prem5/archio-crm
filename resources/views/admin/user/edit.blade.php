@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/users')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">User Edit Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/users/'.$user->id,'method'=>'PUT','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text" required="" parsley-type="name" name="name" class="form-control" id="name" placeholder="Name" value="{{old('name')?old('name'):$user->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Email</label>
                                <div class="col-7">
                                    <input type="email"  name="email" parsley-type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{old('email')?old('email'):$user->email}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">User Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="type" id="type" required="" class="form-control" onchange="myFunction(this)">
                                        <option selected>Select user type</option>
                                        <?php
                                        foreach (config('custom.user_types') as $type => $item) {
                                            if ($type == $user->type) {
                                                echo "<option selected value='$type'> $item </option>";
                                            } else {
                                                echo "<option value='$type'> $item </option>";
                                            }
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-4 col-form-label">Status<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="status" id="status" required="" class="form-control">
                                        {{--<option selected>Select status</option>--}}
                                        @if($user->status == '1')
                                            <option value="1" selected>Enable</option>
                                            <option value="2">Disable</option>
                                        @else
                                            <option value="1" >Enable</option>
                                            <option value="2" selected>Disable</option>
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hori-pass1" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input id="hori-pass1" type="password" name="password" placeholder="Password" required="" class="form-control"  value="{{old('password')?old('password'):$user->password}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hori-pass2" class="col-4 col-form-label">Confirm Password
                                    <span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input data-parsley-equalto="#hori-pass1" type="password" name="password_confirmation" required="" placeholder="Password" class="form-control" id="hori-pass2" value="{{old('password_confirmation')?old('password_confirmation'):$user->password}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <div class="checkbox checkbox-purple">
                                        <input id="checkbox6" type="checkbox" data-parsley-multiple="checkbox6">
                                        <label for="checkbox6">
                                            Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        <A href='{{url('myadmin/users')}}'>   Cancel</A>
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
