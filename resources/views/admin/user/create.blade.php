@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/users')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">User Registration Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                            <div class="card-box">
                                <br>
                                {!! Form::open(['url'=>'myadmin/users','method'=>'POST','files'=>true]) !!}
                                    <div class="form-group row">
                                        <label for="name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                                        <div class="col-7">
                                            <input type="text" required="" parsley-type="name" name="name" class="form-control" id="name" placeholder="Name"value="{{old('name')}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-4 col-form-label">Email</label>
                                        <div class="col-7">
                                            <input type="email"  name="email" parsley-type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{old('email')}}">
                                        </div>
                                        @if ($errors->has('email'))
                                            <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                                        @endif
                                    </div>
                                    <div class="form-group row">
                                        <label for="type" class="col-4 col-form-label">User Type<span class="text-danger">*</span></label>
                                        <div class="col-7">
                                            <select name="type" id="type" required="" class="form-control" value="{{old('type')}}">
                                                <option selected>Select user type</option>
                                                @foreach(config('custom.user_types') as $index=>$value)
                                                    @if($index < '6')
                                                       <option value="{{$index}}">{{$value}}</option>
                                                    @endif
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                <div class="form-group row">
                                    <label for="status" class="col-4 col-form-label">Status<span class="text-danger">*</span></label>
                                    <div class="col-7">
                                        <select name="status" id="status" required="" class="form-control" value="{{old('status')}}">
                                            <option selected>Select status</option>
                                            @foreach(config('custom.statuses') as $index=>$value)
                                                   <option value="{{$index}}">{{$value}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                    <div class="form-group row">
                                        <label for="hori-pass1" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                                        <div class="col-7">
                                            <input id="hori-pass1" type="password" name="password" placeholder="Password" required="" class="form-control" value="{{old('password')}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="hori-pass2" class="col-4 col-form-label">Confirm Password
                                            <span class="text-danger">*</span></label>
                                        <div class="col-7">
                                            <input data-parsley-equalto="#hori-pass1" type="password" name="password_confirmation" required="" placeholder="Password" class="form-control" id="hori-pass2" value="{{old('password_confirmation')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-8 offset-4">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-8 offset-4">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                <div class="visible-lg" style="height: 79px;"></div>
                            </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
