@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12"> <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/users')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>

                        <h4 class="page-title">User  :</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">

                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/users','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                              {{$user->name}}


                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Email</label>
                                {{$user->email}}
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">User Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{config('custom.user_types')[$user->type]}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-4 col-form-label">Status<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{config('custom.statuses')[$user->status]}}

                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-4 offset-4">

                                    <button type="button" class="btn btn-info waves-effect waves-light">
                                        <A href='{{url('myadmin/users')}}'>Back</A>
                                    </button>

                                </div>
                            </div>


                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
