@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/users/create')}}" class="btn btn-info waves-effect waves-light" role="button">Add User</a>
                        </div>
                        <h4 class="page-title">List of Users:</h4>
                    </div>
                </div>
                <br>
                @if (Session::has('success_message'))
                    <div class="alert alert-success">{{ Session::get('success_message') }}</div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">


                            <div class="form-inline m-b-20">
                                <div class="row">
                                    <div class="col-md-6 text-xs-center">
                                        <div class="form-group">
                                            <div class="form-group float-right">
                                                <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="on">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table id="demo-foo-filtering" class="table table-striped table-bordered toggle-circle m-b-0 default footable-loaded footable" data-page-size="7">
                                <thead>
                                <tr>
                                    <th data-toggle="true" class="footable-visible footable-first-column footable-sortable">S.N.<span class="footable-sort-indicator"></span></th>
                                    <th data-toggle="true" class="footable-visible footable-first-column footable-sortable">Name<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Email<span class="footable-sort-indicator"></span></th>
                                    <th data-hide="phone" class="footable-visible footable-sortable">User Types<span class="footable-sort-indicator"></span></th>
                                    <th data-hide="phone, tablet" class="footable-visible footable-sortable">Status<span class="footable-sort-indicator"></span></th>
                                    <th data-hide="phone, tablet" class="footable-visible footable-last-column footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($users as $user)
                                    <tr class="footable-even" style="">
                                        <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>{{$loop->iteration}}</td>
                                        <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>{{$user->name}}</td>
                                        <td class="footable-visible">{{$user->email}}</td>
                                        <td class="footable-visible">{{config('custom.user_types')[$user->type]}}</td>
                                        <td class="footable-visible">{{config('custom.statuses')[$user->status]}}</td>
                                        <td class="footable-visible footable-last-column">
                                            <a href="{{url('myadmin/users/'.$user->id.'/edit')}}"><span class="label label-table label-success">Edit</span></a>
                                            <a href="{{url('myadmin/users/'.$user->id)}}"><span class="label btn-warning waves-effect waves-light">Show</span></a>

                                             {!! Form::open([
                                                                    'method'=>'DELETE',
                                                                    'url' => ['myadmin/users', $user->id],
                                                                    'style' => 'display:inline'
                                                                ]) !!}
                                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete ">Delete</span>', array(
                                                    'type' => 'submit',
                                                    'class' => 'label btn-danger btn-xs',
                                                    'name' => 'Dlete',
                                                    'title' => 'Delete',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                {{--<tfoot>--}}
                                {{--<tr class="active">--}}
                                    {{--<td colspan="5" class="footable-visible">--}}
                                        {{--<div class="text-right">--}}
                                            {{--<ul class="pagination pagination-split footable-pagination justify-content-end m-t-10 m-b-0"><li class="footable-page-arrow disabled"><a data-page="first" href="#first">«</a></li><li class="footable-page-arrow disabled"><a data-page="prev" href="#prev">‹</a></li><li class="footable-page active"><a data-page="0" href="#">1</a></li><li class="footable-page"><a data-page="1" href="#">2</a></li><li class="footable-page"><a data-page="2" href="#">3</a></li><li class="footable-page"><a data-page="3" href="#">4</a></li><li class="footable-page"><a data-page="4" href="#">5</a></li><li class="footable-page-arrow"><a data-page="next" href="#next">›</a></li><li class="footable-page-arrow"><a data-page="last" href="#last">»</a></li></ul>--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--</tfoot>--}}
                            </table>
                            {!! $users !!}
                        </div>
                    </div>
                </div>









                <!-- end row -->

            </div> <!-- container -->
        </div>
    </div>


@endsection