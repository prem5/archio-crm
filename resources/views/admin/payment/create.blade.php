@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/payment/clients')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Client Payment Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/payment/clients','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date</label>
                                <div class="col-7">
                                    <input type="text" required="" name="date"class="form-control datepicker"  id="date" placeholder="Select  date" value="{{old('date')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Title</label>
                                <div class="col-7">
                                    <input type="text"  name="title" parsley-type="amount" class="form-control" id="title" value="{{old('title')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="project_id" id="project_id" required="" class="form-control">
                                        <option selected>Select user type</option>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}">{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="client_id" class="col-4 col-form-label">Client Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="client_id" id="client_id" required="" class="form-control">
                                        <option selected>Select vendor</option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Amount</label>
                                <div class="col-7">
                                    <input type="number"  name="amount" parsley-type="amount" class="form-control" id="inputEmail3" placeholder="0.00" required value="{{old('amount')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_type" class="col-4 col-form-label">Payment Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="payment_type" id="payment_type" required="" class="form-control">
                                        <option selected>Select payment type</option>
                                        @foreach(config('custom.payment_types') as $index=>$value)
                                            <option value="{{$index}}">{{$value}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="remark" class="col-4 col-form-label">Remark:</label>
                                <div class="col-7">
                                    <textarea rows="4" cols="50" name="remark" value="{{old('remark')}}"> </textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Document</label>
                                <div class="col-7">
                                    <input type="file"  name="image" parsley-type="amount" class="form-control" id="inputEmail3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Add
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
