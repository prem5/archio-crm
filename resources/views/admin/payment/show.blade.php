@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/payment/clients')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Payment Show:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/payments','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$payment->project->name}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Title</label>
                                <div class="col-7">
                                    {{$payment->title}}      </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Amount</label>
                                <div class="col-7">
                                    {{$payment->amount}}  </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_type" class="col-4 col-form-label">Payment Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$payment->payment_type}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date</label>
                                <div class="col-7">
                                    {{$payment->date}}        </div>
                            </div>
                            <div class="form-group row">
                                <label for="remark" class="col-4 col-form-label">Remark:</label>
                                <div class="col-7">
                                    {{$payment->remark}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Document</label>
                                <div class="col-7">
                                    @if($payment->doc)<a href="{{url($payment->doc)}}" target="_blank">Click</a>@endif      </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="button" class="btn btn-info waves-effect waves-light">
                                        <A href='{{url('myadmin/payment/clients')}}'>Back</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
