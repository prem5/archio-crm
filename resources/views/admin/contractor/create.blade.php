@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/contractors')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Contractor Registration Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/contractors','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="project_id" id="project_id" required="" class="form-control">
                                        <option selected value="">Select project</option>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}">{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('project_id'))
                                        <ul class="parsley-errors-list filled" ><li class="parsley-required">Plz select Project Name</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Contractor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="contractor_id" id="contractor_id" required="" class="form-control">
                                        <option selected>Select contractor</option>
                                        @foreach($contractors as $contractor)
                                            <option value="{{$contractor->id}}">{{$contractor->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('contractor_id'))
                                        <ul class="parsley-errors-list filled" ><li class="parsley-required">Plz select Comtractor Name</li></ul>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Add
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
