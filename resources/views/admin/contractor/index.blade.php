@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/contractors/create')}}" class="btn btn-info waves-effect waves-light" role="button">Add Contractor</a>
                        </div>
                        <h4 class="page-title">List of contractors:</h4>
                    </div>
                </div>
                <br>
                @if (Session::has('success_message'))
                    <div class="alert alert-success">{{ Session::get('success_message') }}</div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">


                            <div class="form-inline m-b-20">
                                <div class="row">
                                    <div class="col-md-6 text-xs-center">
                                        <div class="form-group">
                                            <div class="form-group float-right">
                                                <input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="on">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table id="demo-foo-filtering" class="table table-striped table-bordered toggle-circle m-b-0 default footable-loaded footable" data-page-size="7">
                                <thead>
                                <tr>
                                    <th data-toggle="true" class="footable-visible footable-first-column footable-sortable">S.N.<span class="footable-sort-indicator"></span></th>
                                    <th data-toggle="true" class="footable-visible footable-first-column footable-sortable">Project Name<span class="footable-sort-indicator"></span></th>
                                    <th data-toggle="true" class="footable-visible footable-first-column footable-sortable">Contractor Name<span class="footable-sort-indicator"></span></th>
                                    <th data-hide="phone, tablet" class="footable-visible footable-last-column footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($contractors as $contractor)
                                    <tr class="footable-even" style="">
                                        <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>{{$loop->iteration}}</td>
                                        <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>{{$contractor->project->name}}</td>
                                        <td class="footable-visible">{{$contractor->contractors->name}}</td>
                                        <td class="footable-visible footable-last-column">
                                            <a href="{{url('myadmin/contractors/'.$contractor->id.'/edit')}}"><span class="label label-table label-success">Edit</span></a>
                                            <a href="{{url('myadmin/contractors/'.$contractor->id)}}"><span class="label btn-warning waves-effect waves-light">Show</span></a>
                                            {!! Form::open([
                                                                   'method'=>'DELETE',
                                                                   'url' => ['myadmin/contractors', $contractor->id],
                                                                   'style' => 'display:inline'
                                                               ]) !!}
                                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete ">Delete</span>', array(
                                                    'type' => 'submit',
                                                    'class' => 'label btn-danger btn-xs',
                                                    'name' => 'Dlete',
                                                    'title' => 'Delete',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                <tr class="active">
                                @endforeach
                                </thbody>

                                </tr>
                                </tfoot>
                            </table>
                           {!! $contractors!!}
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container -->
        </div>
    </div>


@endsection