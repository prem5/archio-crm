@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/contractors')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Contractor Edit :</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/contractors/'.$contractor->id,'method'=>'PUT','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="project_id" id="project_id" required="" class="form-control">
                                        <option selected>Select project</option>
                                        @foreach($projects as $project)
                                            <option @if($contractor->project_id == $project->id ) selected @endif value="{{$project->id}}">{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Contractor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="contractor_id" id="contractor_id" required="" class="form-control">
                                        <option selected>Select contractor</option>
                                        @foreach($contractors as $contract)
                                            <option @if($contractor->contractor_id == $contract->id ) selected @endif value="{{$contract->id}}">{{$contract->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        <A href='{{url('myadmin/contractors')}}'>Cancel</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
