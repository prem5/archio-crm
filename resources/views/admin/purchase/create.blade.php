@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/purchases')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Purchase Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/purchases','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text" required="" name="date"class="form-control datepicker"  id="date" placeholder="Select Start date" value="{{old('date')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Purchased By:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    @foreach(config('custom.purchased_by') as $index=>$value)
                                         <input type="radio" required="" name="purchased_by"class="form-control"    value="{{$index}}" onclick="myPurchase(this)">{{$value}}
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group row" id="vendor_type" style="display: none">
                                <label for="name" class="col-4 col-form-label">Vendor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="vendor_id" id="vendor_id" class="form-control" value="{{old('vendor_id')}}">
                                        <option selected>Select user type</option>
                                        @foreach($vendors as $vendor)
                                            <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="staff" style="display: none">
                                <label for="date" class="col-4 col-form-label">Staff Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="staff_name" parsley-type="name" class="form-control" id="inputEmail3"  value="{{old('staff_name')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Good's Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="name" parsley-type="name" class="form-control" id="inputEmail3" required value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="good_type" class="col-4 col-form-label">Good Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="good_type" id="good_type" required="" class="form-control" onchange="myFunction(this)">
                                        <option selected>Select good type</option>
                                        @foreach(config('custom.good_types') as $index=>$value)
                                            <option value="{{$index}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Quantity<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="number"  name="quantity" parsley-type="quantity" step="0.01" min="0" max="10000" class="form-control" id="inputEmail3" required value="{{old('quantity')}}">
                                </div>
                            </div>
                            <div class="form-group row" id="unit" style="display: none">
                                <label for="unit" class="col-4 col-form-label">Unit</label>
                                <div class="col-7">
                                    <input type="text"  name="unit" parsley-type="unit" class="form-control" id="inputEmail3" placeholder="kg,..."  value="{{old('unit')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="rate" class="col-4 col-form-label">Rate</label>
                                <div class="col-7">
                                    <input type="text"  name="rate" parsley-type="name" class="form-control" id="number" value="{{old('rate')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="total_price" class="col-4 col-form-label">Total Price<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="total_price" parsley-type="name" class="form-control" id="inputEmail3" required value="{{old('total_price')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Add
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
@section('script')
    <script>
        function myFunction(btn) {
            if(btn.value == '2'){
                $('#unit').show();
            }else {
                $('#unit').hide();
            }

        }

        function myPurchase(btn) {
            if (btn.value == '1'){
                $('#vendor_type').show();
                $('#staff').hide();
            }else if(btn.value == '2') {
                $('#vendor_type').hide();
                $('#staff').show();
            }else {
                $('#vendor_type').hide();
                $('#staff').hide();
            }
        }
    </script>
@endsection
