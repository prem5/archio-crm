@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/purchases')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Purchase Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/purchases','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date<span class="text-danger">*</span></label>
                                <div class="col-7">
                                  {{$purchase->date}}      </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Vendor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$purchase->vendors->name}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Good's Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$purchase->name}}
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label for="good_type" class="col-4 col-form-label">Good Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{ config("custom.good_types")[$purchase->good_type]}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Quantity<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$purchase->quantity}}  </div>
                            </div>
                            <div class="form-group row" id="unit" style="display: none">
                                <label for="unit" class="col-4 col-form-label">Unit</label>
                                <div class="col-7">
                                    {{$purchase->unit}}        </div>
                            </div>
                            <div class="form-group row">
                                <label for="rate" class="col-4 col-form-label">Rate</label>
                                <div class="col-7">
                                    {{$purchase->rate}}     </div>
                            </div>
                            <div class="form-group row">
                                <label for="total_price" class="col-4 col-form-label">Total Price<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$purchase->total_price}}       </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="button" class="btn btn-info waves-effect waves-light">
                                        <A href='{{url('myadmin/purchases')}}'>Back</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
@section('script')
    <script>
        function myFunction(btn) {
            if(btn.value == '2'){
                $('#unit').show();
            }else {
                $('#unit').hide();
            }

        }
    </script>
@endsection
