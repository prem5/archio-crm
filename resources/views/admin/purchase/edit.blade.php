@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/purchases')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Purchase  UpdateForm:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/purchases/'.$purchase->id,'method'=>'PUT','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="date" parsley-type="name" class="form-control datepicker" id="date" required value="{{old('date')?old('date'):$purchase->date}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Vendor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="vendor_id" id="vendor_id" required="" class="form-control">
                                        <option selected>Select user type</option>
                                        @foreach($vendors as $vendor)
                                            <option @if($purchase->vendor_id == $vendor->id) selected @endif  value="{{$vendor->id}}">{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Good's Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="name" parsley-type="name" class="form-control" id="inputEmail3" required value="{{old('name')?old('name'):$purchase->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="good_type" class="col-4 col-form-label">Good Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="good_type" id="good_type" required="" class="form-control" onchange="myFunction(this)">
                                        <option selected>Select good type</option>

                                        <?php
                                        foreach (config('custom.good_types') as $type => $item) {
                                            if ($type == $purchase->good_type) {
                                                echo "<option selected value='$type'> $item </option>";
                                            } else {
                                                echo "<option value='$type'> $item </option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Quantity<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="number"  name="quantity" parsley-type="quantity" step="0.01" min="0" max="1000" class="form-control" id="inputEmail3" required value="{{old('quantity')?old('quantity'):$purchase->quantity}}">
                                </div>
                            </div>
                            <div class="form-group row" id="unit" style="display: none">
                                <label for="unit" class="col-4 col-form-label">Unit</label>
                                <div class="col-7">
                                    <input type="text"  name="unit" parsley-type="unit" class="form-control" id="inputEmail3" placeholder="kg,..." value="{{old('unit')?old('unit'):$purchase->unit}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="rate" class="col-4 col-form-label">Rate</label>
                                <div class="col-7">
                                    <input type="text"  name="rate" parsley-type="name" class="form-control" id="number" value="{{old('rate')?old('rate'):$purchase->rate}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="total_price" class="col-4 col-form-label">Total Price<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="total_price" parsley-type="name" class="form-control" id="inputEmail3" required  value="{{old('total_price')?old('total_price'):$purchase->total_price}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        <A href='{{url('myadmin/purchases')}}'>Cancel</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
@section('script')
    <script>
        function myFunction(btn) {
            if(btn.value == '2'){
                $('#unit').show();
            }else {
                $('#unit').hide();
            }

        }
    </script>
@endsection
