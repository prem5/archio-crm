@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/payment/vendors')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Vendor Payment Edit Form:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/payment/vendors/'.$vendorp->id,'method'=>'PUT','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text"  name="date" parsley-type="amount" class="form-control datepicker" id="inputEmail3" required value="{{old('date')?old('date'):$vendorp->date}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Title</label>
                                <div class="col-7">
                                    <input type="text"  name="title" parsley-type="amount" class="form-control" id="inputEmail3" value="{{old('title')?old('title'):$vendorp->title}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="project_id" id="project_id" required="" class="form-control">
                                        <option selected>Select user type</option>
                                        @foreach($projects as $project)
                                            <option @if($vendorp->project_id == $project->id) selected @endif value="{{$project->id}}">{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="vendor_id" class="col-4 col-form-label">Vendor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="vendor_id" id="vendor_id" required="" class="form-control">
                                        <option selected>Select vendor</option>
                                        @foreach($vendors as $vendor)
                                            <option @if($vendorp->vendor_id == $vendor->id) selected @endif value="{{$vendor->id}}">{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Amount<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="number"  name="amount" parsley-type="amount" class="form-control" id="inputEmail3" placeholder="0.00" required value="{{old('amount')?old('amount'):$vendorp->amount}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_type" class="col-4 col-form-label">Payment Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="payment_type" id="payment_type" required="" class="form-control">
                                        <option selected>Select payment type</option>
                                        <?php
                                        foreach (config('custom.payment_types') as $type => $item) {
                                            if ($type == $vendorp->payment_type) {
                                                echo "<option selected value='$type'> $item </option>";
                                            } else {
                                                echo "<option value='$type'> $item </option>";
                                            }
                                        }
                                        ?>


                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="remark" class="col-4 col-form-label">Remark:</label>
                                <div class="col-7">
                                    <textarea rows="4" cols="50" name="remark">{{$vendorp->remark}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Document</label>
                                <div class="col-7">
                                    <input type="file"  name="image" parsley-type="amount" class="form-control" id="inputEmail3" value="{{old('image')?old('image'):$vendorp->images}}" >
                                    @if($vendorp->doc)<a href="{{url($vendorp->doc)}}" target="_blank">Click</a>@endif      </div>

                            </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        <A href='{{url('myadmin/payment/vendors')}}'>Cancel</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
