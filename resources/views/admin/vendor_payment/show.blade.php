@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/payment/vendors')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Vendor Payment Show:</h4>
                    </div>
                </div>
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/payment/vendors','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Date<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$vender->date}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-4 col-form-label">Title</label>
                                <div class="col-7">
                                    {{$vender->title}}           </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$vender->project->name}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="vendor_id" class="col-4 col-form-label">Vendor Name<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$vender->vendors->name}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Amount<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$vender->amount}}     </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_type" class="col-4 col-form-label">Payment Type<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$vender->payment_type}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="remark" class="col-4 col-form-label">Remark:</label>
                                <div class="col-7">
                                    {{$vender->remark}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-4 col-form-label">Document</label>
                                <div class="col-7">
                                    @if($vender->doc)<a href="{{url($vender->doc)}}" target="_blank">Click</a>@endif      </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-8 offset-4">
                                    <button type="button" class="btn btn-info waves-effect waves-light">
                                        <A href='{{url('myadmin/payment/vendors')}}'>Back</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="visible-lg" style="height: 79px;"></div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
