@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/projects')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title">Project EDIT Form:</h4>
                    </div>
                </div>
                <br>
                @if($errors->any())
                    @foreach($errors as $error)
                        {{$error}}
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/projects/'.$project->id,'method'=>'PUT','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <input type="text" required="" name="name" class="form-control" id="name" placeholder="Enter Name" value="{{old('name')?old('name'):$project->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Client:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="client_id" id="type" required="" class="form-control" onchange="myFunction(this)">
                                        <option selected>Select client</option>
                                        @foreach($clients as $client)
                                            <option @if($project->client_id == $client->id)selected @endif  value="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Project Manager:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    <select name="project_manager" id="type" required="" class="form-control">
                                        <option selected>Select Project Manager</option>
                                        @foreach($managers as $manager)
                                            <option  @if($project->project_manager == $manager->id)  selected @endif value="{{$manager->id}}">{{$manager->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Start_date" class="col-4 col-form-label">Start Date:</label>
                                <div class="col-7">
                                    <input type="date" required="" name=" start_date"class="form-control datepicker"  id="Start_date" placeholder="Enter Start_date" value="{{old('name')?old('name'):$project->start_date}}">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="end_date" class="col-4 col-form-label">End Date:</label>
                                <div class="col-7">
                                    <input type="date" name=" end_date" required="" class="form-control datepicker"  id="end_date" placeholder="Enter end_date"  value="{{old('name')?old('name'):$project->end_date}}">

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-4 col-form-label">Price:</label>
                                <div class="col-7">
                                    <input type="text" name="price"required="" type="price" class="form-control"  id="price" placeholder="Enter price"  value="{{old('name')?old('name'):$project->price}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-4 offset-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                        <A href='{{url('myadmin/projects')}}'>Cancel</A>
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

    </div>


@endsection