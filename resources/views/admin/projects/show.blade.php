@extends('layouts.app')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{url('myadmin/projects')}}" class="btn btn-info waves-effect waves-light" role="button">Back</a>
                        </div>
                        <h4 class="page-title"> Projects  No :  {{$project->id}}</h4>
                    </div>
                </div>
                <br>
                @if($errors->any())
                    @foreach($errors as $error)
                        {{$error}}
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <br>
                            {!! Form::open(['url'=>'myadmin/projects','method'=>'POST','files'=>true]) !!}
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Project Name:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                   {{$project->name}}       </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Client:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$project->client->name}}

                                    {{--<select name="client_id" id="type" required="" class="form-control" onchange="myFunction(this)">--}}
                                        {{--<option selected>Select client</option>--}}
                                        {{--@foreach($clients as $client)--}}
                                            {{--<option value="{{$client->id}}">{{$client->name}}</option>--}}
                                        {{--@endforeach--}}

                                    {{--</select>--}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-4 col-form-label">Project Manager:<span class="text-danger">*</span></label>
                                <div class="col-7">
                                    {{$project->project_managers->name}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Start_date" class="col-4 col-form-label">Start Date:</label>
                                <div class="col-7">
                                    {{$project->start_date}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="end_date" class="col-4 col-form-label">End Date:</label>
                                <div class="col-7">
                                    {{$project->end_date}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-4 col-form-label">Price:</label>
                                <div class="col-7">
                                    {{$project->price}}      </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-4 offset-4">

                                    <button type="button" class="btn btn-info waves-effect waves-light">
                                        <A href='{{url('myadmin/projects')}}'>Back</A>
                                    </button>

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

    </div>


@endsection