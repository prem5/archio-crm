@if ($paginator->hasPages())
    <ul class="pagination pagination-split footable-pagination justify-content-end m-t-10 m-b-0">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="footable-page disabled"><a>&laquo;</a></li>
        @else
            <li class="footable-page"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="footable-page disabled"><a>{{ $element }}</a></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="footable-page active"><a>{{ $page }}</a></li>
                    @else
                        <li class="footable-page"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="footable-page" ><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="footable-page disabled"><a>&raquo;</a></li>
        @endif
    </ul>
@endif
