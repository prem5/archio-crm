<!-- http://coderthemes.com/ubold/light/index.html -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="images/favicon_1.ico">

    <title>{{config('app.name')}}</title>

    <!--Morris Chart CSS -->
{{--<link rel="stylesheet" href="assets/plugins/morris/morris.css">--}}
{{--{!!HTML::style('css/font-awesome.min.css')!!}--}}
{!! HTML:: style('plugins/morris/morris.css')  !!}
{!! HTML:: style('css/bootstrap.min.css')  !!}

{!! HTML:: style('css/icons.css')  !!}

{{--<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />--}}
{!! HTML:: style('css/style.css')  !!}
{{--<link href="assets/css/style.css" rel="stylesheet" type="text/css" />--}}

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    {!! HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') !!}
        <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    {{--<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>--}}

    {!! HTML::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') !!}
    <![endif]-->


    {!! HTML::script('assets/js/modernizr.min.js') !!}

    {{--<script src="assets/js/modernizr.min.js"></script>--}}


    <style type="text/css">
        .topbar{
            background-color: black;
        }
    </style>

</head>




<!-- Begin page -->

<body class="widescreen">

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="card-box">
        <div class="panel-heading">
            <h4 class="text-center"> Sign In to <strong class="text-custom">{{config('app.name')}}</strong></h4>
        </div>
        <div class="p-20">
            {!! Form::open(['url'=>'login','method'=>'POST']) !!}
            {{ csrf_field() }}
                <div class="form-group ">
                    <div class="col-12">
                        <input class="form-control {{ $errors->has('email') ? 'parsley-error' : '' }} " name='email' type="email" required="" placeholder="Email">
                        @if ($errors->has('email'))
                            <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <input class="form-control" name="password" type="password" required="" placeholder="Password">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox" name="remember">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Log In
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
{{--<script src="assets/js/jquery.min.js"></script>--}}
{!! HTML::script('js/jquery.min.js') !!}
{{--<script src="assets/js/tether.min.js"></script>--}}
{!! HTML::script('js/tether.min.js') !!}
<!-- Tether for Bootstrap -->
{{--<script src="assets/js/bootstrap.min.js"></script>--}}
{!! HTML::script('js/bootstrap.min.js') !!}
{{--<script src="assets/js/detect.js"></script>--}}
{!! HTML::script('js/detect.js') !!}
{{--<script src="assets/js/fastclick.js"></script>--}}
{!! HTML::script('js/fastclick.js') !!}
{{--<script src="assets/js/jquery.slimscroll.js"></script>--}}
{!! HTML::script('js/jquery.slimscroll.js') !!}
{{--<script src="assets/js/jquery.blockUI.js"></script>--}}
{!! HTML::script('js/jquery.blockUI.js') !!}
{{--<script src="assets/js/waves.js"></script>--}}
{!! HTML::script('js/waves.js') !!}
{{--<script src="assets/js/wow.min.js"></script>--}}
{!! HTML::script('js/wow.min.js') !!}
{{--<script src="assets/js/jquery.nicescroll.js"></script>--}}
{!! HTML::script('js/jquery.nicescroll.js') !!}
{{--<script src="assets/js/jquery.scrollTo.min.js"></script>--}}
{!! HTML::script('js/jquery.scrollTo.min.js') !!}

{{--<script src="assets/plugins/peity/jquery.peity.min.js"></script>--}}
{!! HTML::script('plugins/peity/jquery.peity.min.js') !!}

<!-- jQuery  -->
{{--<script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>--}}
{!! HTML::script('plugins/waypoints/lib/jquery.waypoints.js') !!}
{{--<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>--}}
{!! HTML::script('plugins/counterup/jquery.counterup.min.js') !!}

{{--<script src="assets/plugins/morris/morris.min.js"></script>--}}
{!! HTML::script('plugins/morris/morris.min.js') !!}
{{--<script src="assets/plugins/raphael/raphael-min.js"></script>--}}
{!! HTML::script('plugins/raphael/raphael-min.js') !!}

{{--<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>--}}
{!! HTML::script('plugins/jquery-knob/jquery.knob.js') !!}

{{--<script src="assets/pages/jquery.dashboard.js"></script>--}}
{!! HTML::script('pages/jquery.dashboard.js') !!}

{{--<script src="assets/js/jquery.core.js"></script>--}}
{!! HTML::script('js/jquery.core.js') !!}
{{--<script src="assets/js/jquery.app.js"></script>--}}
{!! HTML::script('js/jquery.app.js') !!}

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();

    });
</script>

</body>
