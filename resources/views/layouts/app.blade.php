<!-- http://coderthemes.com/ubold/light/index.html -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="images/favicon_1.ico">

    <title>{{config('app.name')}}</title>

    <!--Morris Chart CSS -->
{{--<link rel="stylesheet" href="assets/plugins/morris/morris.css">--}}
{{--{!!HTML::style('css/font-awesome.min.css')!!}--}}
{!!HTML::style('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')!!}
{!! HTML:: style('plugins/morris/morris.css')  !!}
{!! HTML:: style('css/bootstrap.min.css')  !!}

{!! HTML:: style('css/icons.css')  !!}

{{--<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />--}}
{!! HTML:: style('css/style.css')  !!}
{{--<link href="assets/css/style.css" rel="stylesheet" type="text/css" />--}}

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    {!! HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') !!}
        <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    {{--<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>--}}

    {!! HTML::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') !!}
    <![endif]-->


    {!! HTML::script('js/modernizr.min.js') !!}

    {{--<script src="assets/js/modernizr.min.js"></script>--}}


    <style type="text/css">
        .topbar{
            background-color: black;
        }
    </style>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                {{--<a href="{{url('')}}" class="logo">--}}
                    {{--<span>{{config('app.name')}}</span>--}}
                {{--</a>--}}

                <a href="{{url('')}}" class="logo"><i class="icon-c-logo">A</i><span>Archio</span></a>

                {{--<img src="{{url('images/small.jpg')}}" alt="">--}}
                <!-- Image Logo here -->
                <!--<a href="index.html" class="logo">-->
                <!--<i class="icon-c-logo"> <img src="assets/images/logo_sm.png" height="42"/> </i>-->
                <!--<span><img src="assets/images/logo_light.png" height="20"/></span>-->
                <!--</a>-->
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <nav class="navbar-custom">
            <ul class="list-inline float-right mb-0">
                <li class="list-inline-item dropdown notification-list">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <i style="font-size: 35px;" class="md md-account-circle"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Welcome ! {{Auth::user()->name}}</small> </h5>
                        </div>
                        <a href="{{url('logout')}}" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-power"></i> <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="list-inline menu-left mb-0" style="
">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-light waves-effect">
                        <i class="dripicons-menu"></i>
                    </button>
                </li>
            </ul>
        </nav>

    </div>
    <!-- Top Bar End -->

    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>

                    <li class="text-muted menu-title">Navigation</li>
                    @if(Auth::check() && (Auth::user()->type == array_search('Admin',config('custom.user_types'))))
                        <li class="has_sub">
                            <a href="{{url('myadmin/projects')}}" ><i class="ti-home"></i> <span> Projects </span> <span class="menu-arrow"></span></a>

                        </li>

                        <li class="has_sub">
                            <a href="{{url('myadmin/users')}}" class="waves-effect"><i class="ti-user"></i> <span>User</span> <span class="menu-arrow"></span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="ti-credit-card"></i> <span> Payments </span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="{{url('myadmin/payment/clients')}}">Client</a></li>
                                <li><a href="{{url('myadmin/payment/vendors')}}">Vendor</a></li>
                                <li><a href="{{url('myadmin/payment/contractors')}}">Contractor</a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="{{url('myadmin/contractors')}}" class="waves-effect"><i class="ti-user"></i> <span>Contractor</span> <span class="menu-arrow"></span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="{{url('myadmin/purchases')}}" class="waves-effect"><i class="ti-shopping-cart"></i> <span>Purchase</span> <span class="menu-arrow"></span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="{{url('myadmin/good_supplies')}}" class="waves-effect"><i class="ti-paint-bucket"></i> <span>Goods Supply</span> <span class="menu-arrow"></span> </a>
                        </li>
                    @endif
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

@yield('content')

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>
<!-- Insert this line above script imports -->
<script>if (typeof module === 'object') {window.module = module; module = undefined;}</script>
<!-- jQuery  -->
{{--<script src="assets/js/jquery.min.js"></script>--}}
{!! HTML::script('js/jquery.min.js') !!}
{{--<script src="assets/js/tether.min.js"></script>--}}
{!! HTML::script('js/tether.min.js') !!}
<!-- Tether for Bootstrap -->
{{--<script src="assets/js/bootstrap.min.js"></script>--}}
{!! HTML::script('js/bootstrap.min.js') !!}
{{--<script src="assets/js/detect.js"></script>--}}
{!! HTML::script('js/detect.js') !!}
{{--<script src="assets/js/fastclick.js"></script>--}}
{!! HTML::script('js/fastclick.js') !!}
{{--<script src="assets/js/jquery.slimscroll.js"></script>--}}
{!! HTML::script('js/jquery.slimscroll.js') !!}
{{--<script src="assets/js/jquery.blockUI.js"></script>--}}
{!! HTML::script('js/jquery.blockUI.js') !!}
{{--<script src="assets/js/waves.js"></script>--}}
{!! HTML::script('js/waves.js') !!}
{{--<script src="assets/js/wow.min.js"></script>--}}
{!! HTML::script('js/wow.min.js') !!}
{{--<script src="assets/js/jquery.nicescroll.js"></script>--}}
{!! HTML::script('js/jquery.nicescroll.js') !!}
{{--<script src="assets/js/jquery.scrollTo.min.js"></script>--}}
{!! HTML::script('js/jquery.scrollTo.min.js') !!}

{{--<script src="assets/plugins/peity/jquery.peity.min.js"></script>--}}
{!! HTML::script('plugins/peity/jquery.peity.min.js') !!}

<!-- jQuery  -->
{{--<script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>--}}
{!! HTML::script('plugins/waypoints/lib/jquery.waypoints.js') !!}
{{--<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>--}}
{!! HTML::script('plugins/counterup/jquery.counterup.min.js') !!}

{{--<script src="assets/plugins/morris/morris.min.js"></script>--}}
{{--{!! HTML::script('plugins/morris/morris.min.js') !!}--}}
{{--<script src="assets/plugins/raphael/raphael-min.js"></script>--}}
{!! HTML::script('plugins/raphael/raphael-min.js') !!}

{{--<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>--}}
{!! HTML::script('plugins/jquery-knob/jquery.knob.js') !!}

{{--<script src="assets/pages/jquery.dashboard.js"></script>--}}
{{--{!! HTML::script('pages/jquery.dashboard.js') !!}--}}

{{--<script src="assets/js/jquery.core.js"></script>--}}
{!! HTML::script('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('plugins/bootstrap-daterangepicker/daterangepicker.js') !!}
{!! HTML::script('js/jquery.core.js') !!}
{{--<script src="assets/js/jquery.app.js"></script>--}}
{!! HTML::script('js/jquery.app.js') !!}
@yield('script')

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();

        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            dateFormat: "yy-m-d"
        });

    });
</script>



<!-- Insert this line after script imports -->
<script>if (window.module) module = window.module;</script>



</body>


