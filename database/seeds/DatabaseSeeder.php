<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UserTypeSeeder::class);
//        $this->call(ContractorTypeSeeder::class);
//        $this->call(ProductTableSeeder::class);
//        $this->call(UnitsTableSeeder::class);
    }
}
