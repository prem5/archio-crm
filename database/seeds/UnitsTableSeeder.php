<?php

use Illuminate\Database\Seeder;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            ['name'=>'Countable','good_type'=>'1'],
            ['name'=>'K.G.','good_type'=>'2'],
            ['name'=>'Litter','good_type'=>'2'],
            ['name'=>'Meter','good_type'=>'2'],
        ]);
    }
}
