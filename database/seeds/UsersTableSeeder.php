<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            'name'=>'Admin',
            'email' =>'admin@localhost.com',
            'password' => bcrypt('password'),
            'type' =>'6',
            'status'=>'1'
        ]

    ]);

    }
}
