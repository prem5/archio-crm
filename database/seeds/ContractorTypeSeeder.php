<?php

use Illuminate\Database\Seeder;

class ContractorTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contractor_types')->insert([
            ['name'=>'light'],
            ['name'=>'painter'],
            ['name'=>'tiles'],
            ['name'=>'constructor']
        ]);
    }
}
