<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorVatBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_vat_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('contractor_id')->unsigned();
            $table->foreign('contractor_id')->references('id')->on('users');
            $table->date('date');
            $table->string('vat_bill_no');
            $table->string('transection_code');
            $table->enum('payment_type',['1','2','3']);
            $table->float('debit',20,3);
            $table->float('credit',20,3);
            $table->float('amount',20,3);
            $table->string('title')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_vat_bills');
    }
}
