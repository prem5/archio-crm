<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorVatChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_vat_cheques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contractor_vat_bill_id')->unsigned();
            $table->foreign('contractor_vat_bill_id')->references('id')->on('contractor_vat_bills');
            $table->string('bank_name');
            $table->string('cheque_no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_vat_cheques');
    }
}
