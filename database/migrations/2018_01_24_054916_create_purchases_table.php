<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('product_id');
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')->references('id')->on('users');
            $table->date('date');
            $table->float('quantity', 20, 3);
            $table->float('rate',20,3)->nullable();
            $table->float('total_price',20,3);
            $table->float('debit',20,3)->nullable();
            $table->float('credit',20,3)->nullable();
            $table->float('discount',20,3)->nullable();
            $table->integer('purchased_by');
            $table->string('staff_name')->nullable();
            $table->string('transection_code');
            $table->string('lf')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
