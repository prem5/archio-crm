<?php
return [
    'user_types'=>[
        '1'=>'Member',
        '2'=>'Project Manager',
        '3'=>'Client',
        '4'=>'Vendor',
        '5'=>'Contractor',
        '6'=>'Admin'
    ],
    'statuses'=>[
        '1'=>'Enable',
        '2'=>'Disable',
    ],
    'per_pages'=>5,
    'payment_types'=>[
        '1'=>'Cash',
        '2'=>'Check',
        '3'=>'others',
    ],
    'good_types'=>[
        '1'=>'countable',
        '2'=>'measurable'
    ],
    'payment_nature'=>[
        '1'=>'Income',
        '2'=>'Outcome'
    ],
    'purchased_by'=>[
        '1'=>'Vendor',
        '2'=>'Staff'
    ],
    'transection_type'=>[
        'VP'=>"Vendor payment",
        'CTP'=>"Contractor payment",
        'CLP'=>"Clint payment",

    ]


]
?>