<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractorPayment extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id','amount','payment_type','date','remark','contractor_id','user_id','payment_nature','title','doc','cheque_number','bank_name','transection_code'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class,'project_id');
    }

    public function contractor()
    {
        return $this->belongsTo(User::class,'contractor_id');
    }
}
