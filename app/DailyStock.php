<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyStock extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function purchase()
    {
        return $this->belongsTo(Purchase::class,'transection_code','transection_code');
    }
    public function project_goods()
    {
        return $this->belongsTo(ProjectGoods::class,'transection_code','transection_code');
    }
    public function goods_return()
    {
        return $this->belongsTo(GoodReturn::class,'transection_code','transection_code');
    }
}
