<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyCount extends Model
{
    protected $fillable = ['date','count'];
}
