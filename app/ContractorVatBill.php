<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractorVatBill extends Model
{
    use SoftDeletes;

    public function contractor_vat_cheque()
    {
        return $this->hasOne(ContractorVatCheque::class,'contractor_vat_bill_id');
    }

    public function contractor()
    {
        return $this->belongsTo(User::class,'contractor_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
