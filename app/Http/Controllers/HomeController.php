<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function getLogin()
    {
        if(Auth::check()){
            return redirect('myadmin');
        }
        elseif(Auth::viaRemember()){
            return redirect('myadmin');
        }
        else{
            return view('auth.login');
        }
    }

    public function postLogin()
    {
        $this->validate(\request(),[
            'email'=>'required|email',
            'password'=>'required'
        ]);
       if (Auth::attempt(['email'=>\request('email'),'password'=>\request('password'),'status'=>1],\request('remember'))){
            Auth::user()->api_token = time().str_random(50);
            Auth::user()->save();
            $token = Auth::user()->api_token;
            $user = Auth::user();
            Auth::logout();
            return response()->json(['token'=>$token,'login_status'=>'success','user'=>$user],200);
       }
            return response()->json(['error'=>"Invalid credentail",'login_status'=>'invalid'],401);
//       return redirect()->back()->withErrors(['email' =>['Invalid Credentials']]);
    }

    public function getLogout()
    {
        $user = Auth::guard('api')->user();
        if ($user){
            $user->api_token = null;
            $user->save();
        }
        return response()->json(['data'=>'logout_success'],200);
    }

    public function check_user(){
        if(Auth::guard('api')->check()){
            Auth::guard('api')->user()->api_token = time().str_random(50);
            Auth::guard('api')->user()->save();
            $token = Auth::user()->api_token;
            $user = Auth::user();
            return response()->json(['token'=>$token,'login_status'=>'success','user'=>$user],200);
        }
    }
}
