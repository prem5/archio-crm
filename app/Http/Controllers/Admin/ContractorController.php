<?php

namespace App\Http\Controllers\Admin;

use App\Contractor;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $view = 'admin.contractor.';
    protected $redirect = 'myadmin/contractors';
    public function index()
    {
        $contractors = Contractor::whereHas('project',function ($q){
            $q->where('status','1');
        })->with('contractors')
            ->with('project')->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $contractors = $contractors->offset($offset)->limit($limit)->get();
        $total = Contractor::whereHas('project',function ($q){
            $q->where('status','1');
        })->count();
        return response()->json(['contractors'=>$contractors,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::where('status','1')->get();
        $contractors = User::where('type','5')
                            ->where('status','1')
                            ->get();
        return response()->json(['projects'=>$projects,'contractors'=>$contractors],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'project_id'=>'required|numeric',
            'contractor_id'=>'required|numeric'
        ]);
        $contractor = Contractor::firstOrCreate(['project_id'=>\request('project_id'),
                                                'contractor_id'=>\request('contractor_id')]);
        return response()->json(['contractor'=>$contractor],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contractor =Contractor::with('project')->with('contractors')->findOrfail($id);
        return response()->json(['contractor'=>$contractor],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::where('status','1')->get();
        $contractors = User::where('type','5')
            ->where('status','1')
            ->get();
        $contractor =Contractor::with('project')->with('contractors')->findOrfail($id);
        return response()->json(['contractor'=>$contractor,'projects'=>$projects,'contractors'=>$contractors],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'project_id'=>'required',
            'contractor_id'=>'required'
        ]);
        $contractor=Contractor::findOrfail($id);
        $contractor ->project_id =\ request("project_id");
        $contractor -> contractor_id= \request('contractor_id');
        $contractor->save();
        return response()->json(['contractor'=>$contractor],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contractor =Contractor::findOrfail($id);
        $contractor->delete();
        return response()->json(['id'=>$id],200);
    }
}
