<?php

namespace App\Http\Controllers\Admin;

use App\DailyCount;
use App\DailyStock;
use App\GoodReturn;
use App\Product;
use App\Project;
use App\ProjectGoods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GoodsReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = GoodReturn::orderBy('id','DESC');
//            ->with('client')
//            ->with('project_managers')
//            ->orderBy('id','DESC');
        
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $projects = $projects->with('project')->with('product.unit')->offset($offset)->limit($limit)->get();
        $total = GoodReturn::orderBy('id','DESC')->count();
        return response()->json(['goods_returns'=>$projects,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $projects = Project::where('status','1')->join(DB::raw('(SELECT * ,SUM(quantity) FROM `project_goods` GROUP BY project_id , product_id) goods'),function ($q){
//            $q->on('projects.id','goods');
//        })->get();
        $projects = Project::orderBy('id','DESC')->get();
        return response()->json(['projects'=>$projects],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \request()->validate([
            'project_id'=>'required|exists:projects,id',
            'product_id'=>'required|exists:products,id',
            'quantity'=>'required|numeric',
            'return_to'=>'required',
            'date'=>'required',
            'to_project'=>'required_if:return_to,2',
        ]);
        $good_return = new GoodReturn;
        $good_return->project_id = \request('project_id');
        $good_return->product_id = \request('product_id');
        $good_return->quantity = \request('quantity');
        $good_return->return_to = \request('return_to');
        $good_return->date = date('Y-m-d',strtotime(\request('date')));
        $good_return->user_id = Auth::guard('api')->user()->id;
        $good_return->to_project = \request('to_project');
        $dailyCount = DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
        if ($dailyCount->count > 0){
            $dailyCount->count = $dailyCount->count + 1;
        }else {
            $dailyCount->count = 1;
        }
        if ($dailyCount->save()){
            $good_return->transection_code = date('Ymd').$dailyCount->count.'GR';
            if($good_return->save()){

                    $daily_stock = new DailyStock;
                    if (DailyStock::where('product_id',\request('product_id'))->orderBy('id','DESC')->count() < 1){
                        $daily_stock_old = new DailyStock;
                        $daily_stock_old->added_on = date('Y-m-d');
                        $daily_stock_old->product_id = \request('product_id');
                        $daily_stock_old->user_id = Auth::guard('api')->user()->id;
                        $daily_stock_old->quantity = (\request('quantity'));
                        $daily_stock_old->transection_code = $good_return->transection_code;
                        $daily_stock_old->remark = \request('remark');
                        $daily_stock_old->save();
                    }elseif(DailyStock::where('product_id',\request('product_id'))) {
                        $daily_stock_old = DailyStock::where('product_id',\request('product_id'))->orderBy('id','DESC')->first();
                        $daily_stock->added_on = date('Y-m-d');
                        $daily_stock->user_id = Auth::guard('api')->user()->id;
                        $daily_stock->product_id = \request('product_id');
                        $daily_stock->quantity = $daily_stock_old->quantity + \request('quantity');
                        $daily_stock->transection_code = $good_return->transection_code;
                        $daily_stock->remark = "Goods return from project";
                        $daily_stock->save();
                    }
                if($good_return->return_to == 2){ // sending goods to another project
                    $dailyCount = DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
                    $dailyCount->count = $dailyCount->count + 1;
                    if($dailyCount->save()){
                        $transection_code = date('Ymd').$dailyCount->count.'PG';
                        $project_good = new  ProjectGoods;
                        $project_good->project_id = \request('to_project');
                        $project_good->product_id = \request('product_id');
                        $project_good->quantity = \request('quantity');
                        $project_good->date = date('Y-m-d',strtotime(\request('date')));
                        $project_good->transection_code = $transection_code;
                        $project_good->staff_name = Auth::guard('api')->user()->name;
                        if($project_good->save()){
                            $daily_stock = new DailyStock;
                            $daily_stock_old = DailyStock::where('product_id',\request('product_id'))->orderBy('id','DESC')->first();
                            $daily_stock->added_on = date('Y-m-d');
                            $daily_stock->user_id = Auth::guard('api')->user()->id;
                            $daily_stock->product_id = \request('product_id');
                            $daily_stock->quantity = $daily_stock_old->quantity - \request('quantity');
                            $daily_stock->transection_code = $project_good->transection_code;
                            $daily_stock->remark = "Good supplied to project";
                            $daily_stock->save();
                        }
                    }
                }
                return response()->json(['goods_return'=>$good_return],200);
            }
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getGoods($id)
    {
        $goods = ProjectGoods::where('project_id',$id)->with('product.unit')->get();
        $good_returns = GoodReturn::where('project_id',$id)->get();
        return response()->json(['goods'=>$goods,'good_returns'=>$good_returns],200);
    }
}
