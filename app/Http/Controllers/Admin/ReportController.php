<?php

namespace App\Http\Controllers\Admin;

use App\DailyBalance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function date_report()
    {
        $start = (\request('start_at'))? \request('start_at') : date('Y-m-d');
        $end = (\request('start_at'))? \request('end_at') : date('Y-m-d');
        $start = date('Y-m-d',strtotime(substr($start,4,12)));
        $end = date("Y-m-d",strtotime(substr($end,4,12)));
        $reports = DailyBalance::where('added_on','<=',$end)
                                ->where('added_on',">=",$start)
                                ->with('project_payment.project')
                                ->with('contractor_payment.project')
                                ->with('contractor_payment.contractor')
                                ->with('vendor_payment.project')
                                ->with('vendor_payment.vendor')
                                ->with('purchase.vendors')
                                ->with('purchase.product')
                                ->with('expense.expense_topic')
                                ->with('vat_bill.project')
                                ->with('vat_bill.client')
                                ->get();
        return response()->json(['reports'=>$reports,'start'=>$start,'end'=>$end],200);
    }
}
