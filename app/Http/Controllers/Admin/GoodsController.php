<?php

namespace App\Http\Controllers\Admin;

use App\DailyCount;
use App\DailyStock;
use App\GoodReturn;
use App\Product;
use App\Project;
use App\ProjectGoods;
use App\Purchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $view = 'admin.good_supply.';
    protected $redirect = 'myadmin/good_supplies';
    public function index()
    {
        $good_supplied = ProjectGoods::whereHas('project',function ($q){
            $q->where('status','1');
        })->with('project')->with('purchase')->with('product')->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $good_supplied = $good_supplied->offset($offset)->limit($limit)->get();
        $total = ProjectGoods::count();
        return response()->json(['good_supplied'=>$good_supplied,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::where('status','1')->get();
        $products = Product::select('products.id as id','products.name as name','stocks.quantity as quantity','units.name as unit')->join(DB::raw('(SELECT * FROM `daily_stocks` WHERE id IN (SELECT MAX(id) FROM daily_stocks GROUP BY product_id) ) stocks'),function ($q){
        $q->on('products.id','stocks.product_id');
    })->join('units','units.id','=','products.unit_id')->where('stocks.quantity','>','0')->get();
        return response()->json(['projects'=>$projects,'purchased_goods'=>$products],200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'date'=>'required|date',
            'project_id'=>'required',
            'goods'=>'required',
//            'quantity'=>'required|numeric',
            'staff_name'=>'required'
        ]);
        $request['date']= date('Y-m-d',strtotime(\request('date')));

        if(\request('goods')){
            $goods = \request('goods');
            foreach (\request('goods') as $key => $value) {
                if(strpos($key,'roduct_id') > 0){
                    $new_key = substr($key,-2,1); // ‌‌substr($key,-2,1);
                    $product_id = $goods["product_id[$new_key]"];
                    $quantity = $goods["quantity[$new_key]"];
                    $request['quantity'] = $quantity;
                    $request['product_id'] = $product_id;

                    $dailyCount = DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
                    if ($dailyCount->count > 0){
                        $dailyCount->count = $dailyCount->count + 1;
                    }else {
                        $dailyCount->count = 1;
                    }
                    if ($dailyCount->save()){
                        $request['transection_code'] = date('Ymd').$dailyCount->count.'PG';

                        $daily_stock = new DailyStock;
                        if (DailyStock::where('product_id',$product_id)->orderBy('id','DESC')->count() < 1){
                            $daily_stock_old = new DailyStock;
                            $daily_stock_old->added_on = date('Y-m-d');
                            $daily_stock_old->product_id = $product_id;
                            $daily_stock_old->user_id = Auth::guard('api')->user()->id;
                            $daily_stock_old->quantity = $quantity;
                            $daily_stock_old->transection_code = $request['transection_code'];
                            $daily_stock->remark = "Good supplied to project";
                            $daily_stock_old->save();
                        }elseif(DailyStock::where('product_id',\request('product_id'))) {
                            $daily_stock_old = DailyStock::where('product_id',$product_id)->orderBy('id','DESC')->first();
                            $daily_stock->added_on = date('Y-m-d');
                            $daily_stock->user_id = Auth::guard('api')->user()->id;
                            $daily_stock->product_id = $product_id; //
                            $daily_stock->quantity = $daily_stock_old->quantity - $quantity;
                            $daily_stock->transection_code = $request['transection_code'];
                            $daily_stock->remark = "Good supplied to project";
                            $daily_stock->save();
                        }
                        $new_goods = ProjectGoods::create(\request()->all());
                }
            }
        }

            return response()->json(['goods'=>$new_goods],200);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $good = ProjectGoods::with('purchase')->with('project')->with('product')->findOrfail($id);
        return response()->json(['good'=>$good],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::where('status','1')->get();
        $purchases = Purchase::all();
        $good = ProjectGoods::findOrfail($id);
        return view($this->view.'edit',compact('good','purchases','projects'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'date'=>'required|date',
            'project_id'=>'required',
            'purchase_id'=>'required',
            'quantity'=>'required|numeric'
        ]);
//        dd($request->all());
        $good =ProjectGoods::findOrfail($id);
        $good->date =  date('Y-m-d',strtotime(\request('date')));
        $good->project_id =  \request('project_id');
        $good->purchase_id =  \request('purchase_id');
        $good->quantity =  \request('quantity');
        $good->save();
        Session::flash('success_message','Goods Updated Succefully');
        return redirect ($this->redirect);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $good =ProjectGoods::findOrfail($id);
        $good->delete();
        Session::flash('success_message','Goods has been Deleted');
        return redirect($this->redirect);

    }

    public function getStockReport()
    {
        $start = (\request('start_at'))? \request('start_at') : date('Y-m-d');
        $end = (\request('start_at'))? \request('end_at') : date('Y-m-d');
        $start = date('Y-m-d',strtotime(substr($start,4,12)));
        $end = date("Y-m-d",strtotime(substr($end,4,12)));
        $reports = DailyStock::where('added_on','<=',$end)
                            ->where('added_on','>=',$start)
                            ->with('user')
                            ->with('product')
                            ->with('purchase.vendors')
                            ->with('purchase.product')
                            ->with('project_goods.project')
                            ->with('project_goods.product')
                            ->with('goods_return.project')
                            ->with('goods_return.product')
                            ->with('goods_return.to_project')
                            ->get();
        return response()->json(['reports'=>$reports,'start'=>$start,'end'=>$end],200);

    }

    public function getProjects()
    {
        $projects = Project::where('status','1')->get();
        return response()->json(['projects'=>$projects],200);
    }

    public function getGoodReport($id)
    {
        $reports = ProjectGoods::where('project_id',$id)->with('project')->with('product.unit')->get();
        $returns = GoodReturn::where('project_id',$id)->with('project')->with('product.unit')->with('to_project')->get();
        $projects = Project::where('status','1')->get();
        return response()->json(['projects'=>$projects,'reports'=>$reports,'returns'=>$returns],200);
    }

    public function getProducts()
    {
        $products = Product::all();
        return response()->json(['products'=>$products],200);
    }

    public function getProductStock($id)
    {
        $reports = DailyStock::where('product_id',$id)
                                ->with('product.unit')
                                ->orderBy('id','DESC')
                                ->first();
        $products = Product::all();
        return response()->json(['reports'=>$reports,'products'=>$products],200);
    }
}






































































