<?php

namespace App\Http\Controllers\Admin;

use App\DailyBalance;
use App\DailyCount;
use App\Project;
use App\User;
use App\VatBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VatBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vat_bills = VatBill::whereHas('project',function ($q){
            $q->where('status','1');
        })->with('client')->with('project')->with('user')->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $vat_bills = $vat_bills->offset($offset)->limit($limit)->get();
        $total = VatBill::count();
        return response()->json(['vat_bills'=>$vat_bills,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = User::where('status','1')
            ->where('type','3')
            ->get();
        $projects = Project::where('status','1')->get();
        return response()->json(['clients'=>$clients,'projects'=>$projects],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'project_id'=>'required',
            'client_id'=>'required',
            'amount'=>'required|numeric',
            'date'=>'required',
            'vat_bill_no'=>'required',
        ]);

        $dailyCount =DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
        if ($dailyCount->count > 0){
            $dailyCount->count = $dailyCount->count + 1;
        }else {
            $dailyCount->count = 1;
        }
        if ($dailyCount->save()){
            $vat_bill = new VatBill;
            $vat_bill->project_id = \request('project_id');
            $vat_bill->client_id = \request('client_id');
            $vat_bill->date = date('Y-m-d',strtotime(substr(\request('date'),4,12)));;
            $vat_bill->title = \request('title');
            $vat_bill->amount = \request('amount');
            $vat_bill->vat_bill_no = \request('vat_bill_no');
            $vat_bill->user_id = Auth::guard('api')->user()->id;
            $vat_bill->transection_code = date('Ymd').$dailyCount->count.'VB';
            if ($vat_bill->save()){
                $daily_balance = new DailyBalance;
                if((DailyBalance::orderBy('id','DESC')->count() < 1)){
                    $daily_balance_old = new DailyBalance;
                    $daily_balance_old->added_on = date('Y-m-d');
                    $daily_balance_old->balance = 0;
                    $daily_balance_old->credit = 0;
                    $daily_balance_old->debit = 0;
                    $daily_balance_old->amount = 0;
                    $daily_balance_old->user_id = Auth::guard('api')->user()->id;
                    $daily_balance_old->transection_code = $vat_bill->transection_code;
                    $daily_balance_old->remark = "Vat bill for client";;
                    $daily_balance_old->save();

                }else{
                    $daily_balance_old = DailyBalance::orderBy('id','DESC')->first();
                    $daily_balance->balance = $daily_balance_old->balance;
                    $daily_balance->amount = $daily_balance_old->amount;
                    $daily_balance->credit = $daily_balance_old->credit ;
                    $daily_balance->debit = $daily_balance_old->debit;
                    $daily_balance->added_on = date('Y-m-d');
                    $daily_balance->remark = "Vat bill for client";
                    $daily_balance->transection_code = $vat_bill->transection_code;
                    $daily_balance->user_id = Auth::guard('api')->user()->id;
                    $daily_balance->save();
                }
            }
        }
        return response()->json(['vat_bill'=>$vat_bill],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vat_bill = VatBill::with('project')->with('client')->with('user')->findOrFail($id);
        return response()->json(['vat_bill'=>$vat_bill],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
