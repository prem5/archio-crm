<?php

namespace App\Http\Controllers\Admin;

use App\DailyBalance;
use App\DailyCount;
use App\DailyStock;
use App\Product;
use App\Purchase;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $view = 'admin.purchase.';
    protected $redirect = 'myadmin/purchases';
    public function index()
    {
        $purchases = Purchase::with('users')->with('vendors')->with('product')->orderBy('id','desc');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $purchases = $purchases->offset($offset)->limit($limit)->get();
        $total = Purchase::count();
        return response()->json(['purchases'=>$purchases,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = User::where('status','1')
                        ->where('type','4')
                        ->get();
        return response()->json(['vendors'=>$vendors],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'date'=>'required',
            'vendor_id'=>'required_if:purchased_by,1',
//            'name'=>'required',
            'product_id'=>'required|exists:products,id',
            'quantity'=>'required',
            'rate'=>'required',
            'total_price'=>'required',
            'purchased_by'=>'required',
            'staff_name'=>'required_if:purchased_by,2'
        ]);


        $dailyCount = DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
        if ($dailyCount->count > 0){
            $dailyCount->count = $dailyCount->count + 1;
        }else {
            $dailyCount->count = 1;
        }
        if ($dailyCount->save()){
            $debit = 0.0;
            $credit = 0.0;
            if (\request('debit')){
                $debit = \request('debit');
            }
            if (\request('credit')){
                $credit = \request('credit');
            }
            $purchase = new Purchase;
            $purchase->user_id = Auth::guard('api')->user()->id;
            $purchase->purchased_by = \request('purchased_by');
            if(\request('staff_name')){
                $purchase->staff_name = \request('staff_name');
            }
            if(\request('vendor_id')){
                $purchase->vendor_id = \request('vendor_id');
            }
            $purchase->date = date('Y-m-d',strtotime(\request('date')));
            $purchase->lf = \request('lf');
            $purchase->product_id = \request('product_id');
            $purchase->quantity = \request('quantity');
            $purchase->rate = \request('rate');
            $purchase->total_price = \request('total_price');
            $purchase->discount = \request('discount');
            $purchase->debit = $debit;
            $purchase->credit = $credit;
            if (\request('purchased_by') == '1'){
                $purchase->transection_code = date('Ymd').$dailyCount->count.'P';
            }
            if (\request('purchased_by') == '2'){
                $purchase->transection_code = date('Ymd').$dailyCount->count.'PS';
            }
            if($purchase->save()){
                $daily_balance = new DailyBalance;
                if((DailyBalance::orderBy('id','DESC')->count() < 1)){
                    $daily_balance_old = new DailyBalance;
                    $daily_balance_old->added_on = date('Y-m-d');
                    $daily_balance_old->balance = -($debit + $credit);
                    $daily_balance_old->credit = $credit;
                    $daily_balance_old->debit = $debit;
                    $daily_balance_old->amount = 0;
                    $daily_balance_old->user_id = Auth::guard('api')->user()->id;
                    $daily_balance_old->transection_code = $purchase->transection_code;
                    $daily_balance_old->remark = "purchase information";;
                    $daily_balance_old->save();

                }else{
                    $daily_balance_old = DailyBalance::orderBy('id','DESC')->first();
                    $daily_balance->balance = $daily_balance_old->balance - ($debit+$credit);
                    $daily_balance->amount = ($daily_balance_old->amount - $debit);
                    $daily_balance->credit = $daily_balance_old->credit + $credit ;
                    $daily_balance->debit = $daily_balance_old->debit + $debit;
                    $daily_balance->added_on = date('Y-m-d');
                    $daily_balance->remark = "purchase information";
                    $daily_balance->transection_code = $purchase->transection_code;
                    $daily_balance->user_id = Auth::guard('api')->user()->id;
                    $daily_balance->save();
                }
                $daily_stock = new DailyStock;
                if (DailyStock::where('product_id',\request('product_id'))->orderBy('id','DESC')->count() < 1){
                    $daily_stock_old = new DailyStock;
                    $daily_stock_old->added_on = date('Y-m-d');
                    $daily_stock_old->product_id = \request('product_id');
                    $daily_stock_old->user_id = Auth::guard('api')->user()->id;
                    $daily_stock_old->quantity = \request('quantity');
                    $daily_stock_old->transection_code = $purchase->transection_code;
                    $daily_stock_old->remark = \request('remark');
                    $daily_stock_old->save();
                }elseif(DailyStock::where('product_id',\request('product_id'))) {
                    $daily_stock_old = DailyStock::where('product_id',$purchase->product_id)->orderBy('id','DESC')->first();
                    $daily_stock->added_on = date('Y-m-d');
                    $daily_stock->user_id = Auth::guard('api')->user()->id;
                    $daily_stock->product_id = \request('product_id');
                    $daily_stock->quantity = $daily_stock_old->quantity + \request('quantity');
                    $daily_stock->transection_code = $purchase->transection_code;
                    $daily_stock->remark = "purchase information";
                    $daily_stock->save();
                }
            }
        }
        return response()->json(['purchase'=>$purchase],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = Purchase::with('users')->with('vendors')->with('product')->findOrfail($id);
        $vendors = User::where('status','1')
            ->where('type','4')
            ->get();
        return response()->json(['purchase'=>$purchase,'vendors'=>$vendors],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendors = User::where('status','1')
            ->where('type','4')
            ->get();
        $purchase = Purchase::findOrfail($id);
        return view ($this->view.'edit',compact('purchase' ,'vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'date'=>'required|date',
            'vendor_id'=>'required',
            'name'=>'required',
            'good_type'=>'required',
            'quantity'=>'required',
            'rate'=>'required',
            'total_price'=>'required'
        ]);
//        dd($request->all());
        $purchase = Purchase::findOrfail($id);
        $purchase->date =date('Y-m-d',strtotime(\request('date')));
        $purchase->vendor_id = \request('vendor_id');
        $purchase->vendor_id = \request('vendor_id');
        $purchase->name = \request('name');
        $purchase->good_type = \request('good_type');
        $purchase->quantity = \request('quantity');
        $purchase->unit = \request('unit');
        $purchase->rate = \request('rate');
        $purchase->total_price = \request('total_price');
        $purchase->save();
        Session::flash('success_message'," Purchase Updated Succefully");
        return redirect($this->redirect);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchase = Purchase::findOrfail($id);
        $purchase ->delete();
        Session::flash('success_message','Purchase delete Succefully');
        return redirect($this->redirect);
    }

    public function getVendors()
    {
        $vendors = User::where('status','1')
            ->where('type','4')
            ->get();
        $products = Product::with('unit')->get();
        return response()->json(['vendors'=>$vendors,'products'=>$products],200);
    }
}
