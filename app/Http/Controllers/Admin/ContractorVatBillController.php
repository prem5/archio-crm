<?php

namespace App\Http\Controllers\Admin;

use App\ContractorVatBill;
use App\ContractorVatCheque;
use App\DailyCount;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContractorVatBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vats = ContractorVatBill::whereHas('contractor',function ($q){
            $q->where('type','5');
        })->with('user')->with('contractor')->with('contractor_vat_cheque')->orderBy('id','DESC');
//        return response()->json(['data'=>$contractors]);
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $vats = $vats->offset($offset)->limit($limit)->get();
        $total = ContractorVatBill::count();
        return response()->json(['vats'=>$vats,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contractors = User::where('type','5')->where('status','1')->get();
        return response()->json(['contractors'=>$contractors],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'contractor_id'=>'required',
            'date'=>'required',
            'vat_bill_no'=>'required',
            'payment_type'=>'required',
            'debit'=>'required',
            'credit'=>'required',
            'amount'=>'required',
            'bank_name'=>'required_if:payment_type,2',
            'cheque_no'=>'required_if:payment_type,2'
        ]);

        $dailyCount =DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
        if ($dailyCount->count > 0){
            $dailyCount->count = $dailyCount->count + 1;
        }else {
            $dailyCount->count = 1;
        }

        if ($dailyCount->save()){
            $vat = new ContractorVatBill;
            $vat->contractor_id = \request('contractor_id');
            $vat->amount = \request('amount');
            $vat->debit = \request('debit');
            $vat->credit = \request('credit');
            $vat->date = date('Y-m-d',strtotime(substr(\request('date'),4,12)));;
            $vat->payment_type = \request('payment_type');
            $vat->title = \request('title');
            $vat->vat_bill_no = \request('vat_bill_no');
            $vat->user_id = Auth::guard('api')->user()->id;
            $vat->transection_code = date('Ymd').$dailyCount->count.'CV';
            if ($vat->save()){
                if (\request('payment_type') == '2'){
                    $cheque = new ContractorVatCheque;
                    $cheque->contractor_vat_bill_id = $vat->id;
                    $cheque->bank_name = \request('bank_name');
                    $cheque->cheque_no = \request('cheque_no');
                    $cheque->save();
                }
            }
            return response()->json(['vat'=>$vat],200);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vat = ContractorVatBill::with('contractor')
                                                ->with('user')
                                                ->with('contractor_vat_cheque')
                                                ->findOrFail($id);
        return response()->json(['vat'=>$vat],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vat = ContractorVatBill::with('contractor')
            ->with('user')
            ->with('contractor_vat_cheque')
            ->findOrFail($id);
        $contractors = User::where('type','5')->where('status','1')->get();
        return response()->json(['vat'=>$vat,'contractors'=>$contractors],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return response()->json(['data'=>\request()->all()]);

        $this->validate(\request(),[
            'contractor_id'=>'required',
            'date'=>'required',
            'vat_bill_no'=>'required',
            'payment_type'=>'required',
            'debit'=>'required',
            'credit'=>'required',
            'amount'=>'required',
            'bank_name'=>'required_if:payment_type,2',
            'cheque_no'=>'required_if:payment_type,2'
        ]);
        $vat = ContractorVatBill::findOrFail($id);
        $vat->contractor_id = \request('contractor_id');
        $vat->amount = \request('amount');
        $vat->debit = \request('debit');
        $vat->credit = \request('credit');
        $vat->date = date('Y-m-d',strtotime(substr(\request('date'),4,12)));;
        $vat->payment_type = \request('payment_type');
        $vat->title = \request('title');
        $vat->vat_bill_no = \request('vat_bill_no');

        if (\request('payment_type') == '2'){

            if($vat->contractor_vat_cheque){
                $cheque = ContractorVatCheque::findOrFail($vat->contractor_vat_cheque->id);
                $cheque->bank_name = \request('bank_name');
                $cheque->cheque_no = \request('cheque_no');
                $cheque->save();
            } else{
                $cheque = new ContractorVatCheque;
                $cheque->bank_name = \request('bank_name');
                $cheque->cheque_no = \request('cheque_no');
                $cheque->save();
            }
        }

        if ((\request('payment_type') == '1') || (\request('payment_type' )=='3')){
            if ($vat->contractor_vat_cheque){
                $vat->contractor_vat_cheque->delete();
            }
        }
        $vat->save();
        return response()->json(['vat'=>$vat],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getContractor()
    {
        return response()->json(['data'=>'hi']);
    }
}
