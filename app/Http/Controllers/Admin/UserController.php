<?php

namespace App\Http\Controllers\Admin;

use App\ContractorInfo;
use App\ContractorType;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $view = 'admin.user.';
    protected $redirect = 'myadmin/users';
    public function index()
    {
        $users = User::select('id','name','email','type','address','contact_no','vat_no','status')->where('status','1')
                    ->where('type','<','6')
                    ->with('contractor_infos.contractor_type')
                    ->with('user_type')
                    ->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $users = $users->offset($offset)->limit($limit)->get();
        $total = User::where('status','1')->where('type','<','6')->count();
        return response()->json(['users'=>$users,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(\request(),[
            'name'=>'required',
            'status'=>'required|in:1,2',
            'type'=>'required|numeric',
            'vat_no'=>'required_if:user_type,3,4',
            'contractor_type_id'=>'required_if:type,5',
        ]);
        if(\request('type') == 1){
            \request()->validate([
                'email'=>'required|unique:users',
                'password' => 'required|min:6|confirmed',

            ]);
        }

        $user = User::create(request()->all());
        if(\request('password')){
            $user->password = bcrypt(\request('password'));
            $user->save();
        }
        if (\request('contractor_type_id')){
            $contractor_info = new ContractorInfo;
            $contractor_info->user_id = $user->id;
            $contractor_info->contractor_type_id = \request('contractor_type_id');
            $contractor_info->save();
        }
        return response()->json(['user'=>$user,'status'=>'ok'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('contractor_infos.contractor_type')
                    ->with('user_type')->findOrfail($id);
        return response()->json(['user'=>$user],200);
//        return view($this->view.'show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('contractor_infos.contractor_type')
            ->with('user_type')->findOrfail($id);
        $user_types = UserType::all();
        $contractor_types = ContractorType::all();
        return response()->json(['user'=>$user,'user_types'=>$user_types,'contractor_types'=>$contractor_types],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'name'=>'required',
            'email'=>"required_if:user_type,1|unique:users,email,$id",
            'status'=>'sometimes|nullable|in:1,2',
            'password' => 'sometimes|nullable|min:6|confirmed',
            'type'=>'required|numeric',
            'vat_no'=>'required_if:user_type,3,4',
            'contractor_type_id'=>'required_if:type,5'
        ]);

        $user = User::findOrfail($id);
        $user->update(\request()->all());
        if(\request('password')){
            $user->password = bcrypt(\request('password'));
            $user->save();
        }
        if (\request('contractor_type_id')){
            $contractor_info_id = $user->contractor_infos->id;
            return response()->json(['user'=>$contractor_info_id]);
            $contractor_info = ContractorInfo::findOrFail($contractor_info_id);
            $contractor_info->user_id = $user->id;
            $contractor_info->contractor_type_id = \request('contractor_type_id');
            $contractor_info->save();
        }
        return response()->json(['user'=>$user],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
//        if ($user->contractor_infos){
//            $user->contractor_infos->delete();
//        }
        return response()->json(['id'=>$id],200);
    }

    public function getUserTypes()
    {
        $user_types = UserType::all();
        $contractor_types = ContractorType::all();
        return response()->json(['user_types'=>$user_types,'contractor_types'=>$contractor_types],200);
    }

    public function getProfile()
    {
        $user = User::where('id',Auth::guard('api')->user()->id)->with('user_type')->first();
        return response()->json(['user'=>$user],200);
    }
}
