<?php

namespace App\Http\Controllers\Admin;

use App\ContractorType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContractorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractor_types = ContractorType::orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $contractor_types = $contractor_types->offset($offset)->limit($limit)->get();
        $total = ContractorType::orderBy('id','DESC')->count();
        return response()->json(['contractor_types'=>$contractor_types,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'name'=>'required|unique:contractor_types,name'
        ]);
        $contractor_type = new ContractorType;
        $contractor_type->name = \request('name');
        $contractor_type->save();
        return response()->json(['contractor_type'=>$contractor_type],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contractor_type = ContractorType::findOrFail($id);
        return response()->json(['contractor_type'=>$contractor_type],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'name'=>'required|unique:contractor_types,name'
        ]);
        $contractor_type = ContractorType::findOrFail($id);
        $contractor_type->name = \request('name');
        $contractor_type->save();
        return response()->json(['contractor_type'=>$contractor_type],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $contractor_type = ContractorType::findOrFail($id);
//        $contractor_type->delete();
//        return response()->json(['id'=>$id],200);
    }
}
