<?php

namespace App\Http\Controllers\Admin;

use App\DailyBalance;
use App\Project;
use App\ProjectPayment;
use App\User;
use App\VatBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $view = 'admin.projects.';
    protected $redirect = 'myadmin/projects';

    public function index()
    {
        $projects = Project::where('status','1')
                            ->with('client')
                            ->with('project_managers')
                            ->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $projects = $projects->offset($offset)->limit($limit)->get();
        $total = Project::where('status','1')->count();
        return response()->json(['projects'=>$projects,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = User::where('type','3')
            ->where('status','1')
            ->get();
        $managers = User::where('type','2')
                         ->where('status','1')
                         ->get();
        return view('admin.projects.create',compact('clients','managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'name'=>'required|unique:projects,name',
            'client_id'=>'required',
            'project_manager'=>'required',
        ]);
        $request['user_id'] = Auth::user()->id;
        $request['status'] = '1';
        if(\request('start_date')){
            $request['start_date'] =  date('Y-m-d',strtotime(\request('start_date')));
        }
        if(\request('end_date')){
            $request['end_date'] = date('Y-m-d',strtotime(\request('end_date')));
        }
        $project = Project::create($request->all());
        return response()->json(['project'=>$project]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::with('client')
                            ->with('project_managers')
                            ->findOrfail($id);
        return response()->json(['project'=>$project],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clients = User::where('type','3')
            ->where('status','1')
            ->get();
        $managers = User::where('type','2')
            ->where('status','1')
            ->get();
        $project = Project::with('client')
            ->with('project_managers')
            ->findOrfail($id);
        return response()->json(['project'=>$project,'clients'=>$clients,'managers'=>$managers],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'name'=>'required',
            'client_id'=>'required',
            'project_manager'=>'required',
        ]);
        $project = Project::findOrfail($id);
        $project->name = \request('name');
        $project->client_id  =  \request('client_id');
        $project->project_manager = \request('project_manager');
        $project->start_date = \request('start_date');
        $project->end_date = \request('end_date');
        $project->price = \request('price');
        if(\request('start_date')){
           $project->start_date =  date('Y-m-d',strtotime(\request('start_date')));
        }
        if(\request('end_date')){
            $project->end_date = date('Y-m-d',strtotime(\request('end_date')));
        }
        $project ->save();
        return response()->json(['project'=>$project],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();
        return response()->json(['id'=>$id],200);
    }

    public function get_client_managers()
    {
        $clients = User::where('type','3')
            ->where('status','1')
            ->get();
        $managers = User::where('type','2')
            ->where('status','1')
            ->get();
        return response()->json(['clients'=>$clients,'managers'=>$managers],200);
    }

    public function getProjects()
    {
        $projects = Project::all();
        return response()->json(['projects'=>$projects],200);
    }

    public function getProjectReport($id)
    {
       $project_payments = ProjectPayment::where('project_id',$id)
                                            ->with('client')
                                            ->with('project')
                                            ->get();
       $transections = DailyBalance::whereHas('project_payment',function ($q) use ($id){
           $q->where('project_id',$id);
       })->orWhereHas('contractor_payment',function ($t) use ($id){
           $t->where('project_id',$id);
       })->orWhereHas('vendor_payment',function ($query) use ($id){
           $query->where('project_id',$id);
       })->with('project_payment.project')->with('contractor_payment.project')->with('contractor_payment.contractor')->with('vendor_payment.project')->with('vendor_payment.vendor')->get();
       return response()->json(['project_payments'=>$project_payments,'transections'=>$transections],200);
    }
    public function getClientReport($id)
    {

       $transections = DailyBalance::whereHas('project_payment',function ($q) use ($id){
           $q->where('client_id',$id);
       })
           ->orWhereHas('vat_bill',function ($q) use ($id){
               $q->where('client_id',$id);
           })
            ->with('project_payment.project')
           ->with('contractor_payment.project')
           ->with('contractor_payment.contractor')
           ->with('vendor_payment.project')
           ->with('vendor_payment.vendor')
           ->with('vat_bill.project')
           ->with('vat_bill.client')
           ->get();

//        $transections = DailyBalance::whereHas('project_payment', function ($q) use ($id){
//            $q->where('client_id',$id);
//        });
       return response()->json(['transections'=>$transections],200);
    }
    public function getContractorReport($id)
    {

        $transections = DailyBalance::whereHas('contractor_payment',function ($q) use ($id){
            $q->where('contractor_id',$id);
        })->with('project_payment.project')
            ->with('contractor_payment.project')
            ->with('contractor_payment.contractor')
            ->with('vendor_payment.project')
            ->with('vendor_payment.vendor')
            ->get();
        return response()->json(['transections'=>$transections],200);
    }
    public function getVendorReport($id)
    {
        $transections = DailyBalance::whereHas('vendor_payment',function ($q) use ($id){
            $q->where('vendor_id',$id);
        })->with('project_payment.project')->with('contractor_payment.project')->with('contractor_payment.contractor')->with('vendor_payment.project')->with('vendor_payment.vendor')->get();
        return response()->json(['transections'=>$transections],200);
    }
}
