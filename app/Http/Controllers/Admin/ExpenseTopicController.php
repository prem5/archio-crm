<?php

namespace App\Http\Controllers\Admin;

use App\ExpenseTopic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpenseTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = ExpenseTopic::orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $expenses = $expenses->offset($offset)->limit($limit)->get();
        $total = ExpenseTopic::orderBy('id','DESC')->count();
        return response()->json(['expenses'=>$expenses,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'topic'=>'required'
        ]);
        $expense = new ExpenseTopic;
        $expense->topic = \request('topic');
        $expense->save();
        return response()->json(['expense'=>$expense],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expense = ExpenseTopic::findOrFail($id);
        return response(['expense'=>$expense],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate(\request(),[
            'topic'=>'required'
        ]);
        $expense = ExpenseTopic::findOrFail($id);
        $expense->topic = \request('topic');
        $expense->save();
        return response()->json(['expense'=>$expense],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = ExpenseTopic::findOrFail($id);
        $expense->delete();
        return response()->json(['id'=>$id],200);
    }
}
