<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $projects = Product::orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $projects = $projects->with('unit')->offset($offset)->limit($limit)->get();
        $total = Product::orderBy('id','DESC')->count();
        return response()->json(['products'=>$projects,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    public function getUnits()
    {
        $units = Unit::all();
        return response()->json(['units'=>$units],200);
    }
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'name'=>'required|unique:products,name',
            'unit_id'=>'required|exists:units,id',
        ]);
        $product = new Product;
        $product->name = \request('name');
        $product->unit_id = \request('unit_id');
        $product->save();
        return response()->json(['product'=>$product],200);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json(['id'=>$id],200);
    }

    public function edit($id)
    {
        $units = Unit::all();
        $product = Product::findOrFail($id);
        return response()->json(['units'=>$units,'product'=>$product],200);
    }

    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'name'=>'required|unique:products,name',
            'unit_id'=>'required|exists:units,id',
        ]);
        $product = Product::findOrFail($id);
        $product->name = \request('name');
        $product->unit_id = \request('unit_id');
        $product->save();
        return response()->json(['product'=>$product],200);
    }

}
