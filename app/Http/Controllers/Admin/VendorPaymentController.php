<?php

namespace App\Http\Controllers\Admin;

use App\DailyBalance;
use App\DailyCount;
use App\Project;
use App\ProjectPayment;
use App\User;
use App\VendorPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VendorPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $view = 'admin.vendor_payment.';
    protected $redirect = 'myadmin/payment/vendors';
    public function index()
    {
        $payments = VendorPayment::whereHas('project',function ($q){
            $q->where('status','1');
        })->with('user')->with('project')->with('vendor')->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $payments = $payments->offset($offset)->limit($limit)->get();
        $total = VendorPayment::count();
        return response()->json(['payments'=>$payments,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::where('status','1')->get();
        $vendors = User::where('status','1')
                        ->where('type','4')
                        ->get();
        return response()->json(['vendors'=>$vendors,'projects'=>$projects],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(\request(),[
            'project_id'=>'required',
            'vendor_id'=>'required',
            'amount'=>'required|numeric',
            'payment_type'=>'required',
            'date'=>'required',
            'doc'=>'sometimes|nullable|image|mimes:jpeg,jpg,png| max:1000',
            'cheque_number'=>'required_if:payment_type,2',
            'bank_name'=>'required_if:payment_type,2',
        ]);
        $request['date'] = date('Y-m-d',strtotime(substr(\request('date'),4,12)));
        $request['user_id'] = Auth::user()->id;
        $request['payment_nature']= '2';
        $dailyCount =DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
        if ($dailyCount->count > 0){
            $dailyCount->count = $dailyCount->count + 1;
        }else {
            $dailyCount->count = 1;
        }
        if ($dailyCount->save()){
            $payment = VendorPayment::create(\request()->all());
            if ($request->hasFile('doc')){
                $filename = time().rand(100,999).'.'.request()->file('doc')->getClientOriginalExtension();
                request()->file('doc')->move('uploads/',$filename);
                $payment->doc = url('').'/uploads/'.$filename;
            }
            if (\request('payment_type') == '1'){
                $payment->transection_code = "VP".date('ymd').$dailyCount->count.'C';
            }
            if (\request('payment_type') == '2'){
                $payment->transection_code = "VP".date('ymd').$dailyCount->count.'CHQ';
            }
            if (\request('payment_type') == '3'){
                $payment->transection_code = "VP".date('ymd').$dailyCount->count.'O';
            }
            if($payment->save()){
                $daily_balance = new DailyBalance;
                $daily_balance_old = DailyBalance::orderBy('id','DESC')->first();

                $daily_balance->balance = ($daily_balance_old->balance -  $payment->amount);
                $daily_balance->amount = $daily_balance_old->amount -  $payment->amount;
                $daily_balance->credit = ($daily_balance_old->credit); ;
                $daily_balance->debit = ($daily_balance_old->debit + $payment->amount);
                $daily_balance->added_on = date('Y-m-d');
                $daily_balance->remark = "Payment given to vendor";
                $daily_balance->transection_code = $payment->transection_code;
                $daily_balance->user_id = Auth::guard('api')->user()->id;
                $daily_balance->save();
            }
        }
        return response()->json(['payment'=>$payment],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projects = Project::where('status','1')->get();
        $vendors = User::where('status','1')
            ->where('type','4')
            ->get();
        $payment = VendorPayment::with('user')->with('project')->with('vendor')->findOrfail($id);
        return response()->json(['payment'=>$payment,'projects'=>$projects,'vendors'=>$vendors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::where('status','1')->get();
        $vendors = User::where('status','1')
            ->where('type','4')
            ->get();
        $payment = VendorPayment::with('user')->with('project')->with('vendor')->findOrfail($id);
        return response()->json(['payment'=>$payment,'projects'=>$projects,'vendors'=>$vendors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(\request(),[
            'amount'=>'required|numeric',
            'payment_type'=>'required',
            'date'=>'required|date',
            'doc'=>'sometimes|nullable|image|mimes:jpeg,jpg,png| max:1000',
            'cheque_number'=>'required_if:payment_type,2',
            'bank_name'=>'required_if:payment_type,2',
        ]);
        $payment = VendorPayment::findOrfail($id);
        $payment->cheque_number = \request('cheque_number');
        $payment->bank_name = \request('bank_name');
        $payment->amount = \request('amount');
        $date = substr(\request('date'),4,12);
        $payment->date = date('Y-m-d',strtotime($date));
        $payment->remark = \request('remark');
        $payment->title = \request('title');
        $payment->payment_type = \request('payment_type');
        $payment->cheque_number = \request('cheque_number');
        $payment->cheque_number = \request('cheque_number');
        $request['user_id']= Auth::user()->id;
        if ($request->hasFile('image')){
            if (is_file($payment->doc) && file_exists($payment->doc)){
                unlink($payment->doc);
            }
            $filename = time().rand(100,999).'.'.request()->file('image')->getClientOriginalExtension();
            request()->file('image')->move('uploads/',$filename);
            $request['doc'] = 'uploads/'.$filename;
        }
        if (\request('payment_type') == '1'){
            $payment->transection_code = ProjectPayment::stringSeperator($payment->transection_code);
            $payment->transection_code = $payment->transection_code.'C';
        }
        if (\request('payment_type') == '2'){
            $payment->transection_code = ProjectPayment::stringSeperator($payment->transection_code);
            $payment->transection_code = $payment->transection_code.'CHQ';
        }
        if (\request('payment_type') == '3'){
            $payment->transection_code = ProjectPayment::stringSeperator($payment->transection_code);
            $payment->transection_code = $payment->transection_code.'O';
        }
        $payment->save();
        return response()->json(['payment'=>$payment],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor =VendorPayment::findOrfail($id);
        if (is_file($vendor->doc) && file_exists($vendor->doc)){
            unlink($vendor->doc);
        }
        $vendor->delete();
        return response()->json(['id'=>$id],200);
    }
}
