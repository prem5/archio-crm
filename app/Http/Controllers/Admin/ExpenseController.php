<?php

namespace App\Http\Controllers\Admin;

use App\DailyBalance;
use App\DailyCount;
use App\Expense;
use App\ExpenseTopic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::with('expense_topic')->orderBy('id','DESC');
        $offset = (\request('start')) ? \request('start') : 0 ;
        $limit = (\request('limit')) ? \request('limit') : 10 ;
        $expenses = $expenses->offset($offset)->limit($limit)->get();
        $total = Expense::count();
        return response()->json(['expenses'=>$expenses,'total'=>$total,'limit'=>$limit,'start'=>$offset],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = ExpenseTopic::all();
        return response()->json(['topics'=>$topics],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(\request(),[
            'date'=>'required',
            'debit'=>'required|numeric',
            'credit'=>'required|numeric',
            'topic_id'=>'required_if:title,1',
            'other'=>'required_if:title,2',
            'amount'=>'required|numeric',
        ]);
        $expense = new Expense();
        $expense->added_on = date('Y-m-d',strtotime(substr(\request('date'),4,12)));
        $expense->debit = \request('debit');
        $expense->credit = \request('credit');
        $expense->topic_id = \request('topic_id');
        $expense->others = \request('other');
        $expense->amount = \request('amount');
        $expense->staff_name = \request('staff_name');
        $dailyCount =DailyCount::FirstOrNew(['date'=>date('Y-m-d')]);
        if ($dailyCount->count > 0){
            $dailyCount->count = $dailyCount->count + 1;
        }else {
            $dailyCount->count = 1;
        }
        if ($dailyCount->save()){
            $expense->transection_code = time().$dailyCount->count.'E';
        }
        if($expense->save()){
            $daily_balance = new DailyBalance();
            if((DailyBalance::orderBy('id','DESC')->count() < 1)){
                $daily_balance_old = new DailyBalance;
                $daily_balance_old->added_on = date('Y-m-d');
                $daily_balance_old->balance = 0;
                $daily_balance_old->credit = 0;
                $daily_balance_old->debit = 0;
                $daily_balance_old->amount = 0;
                $daily_balance_old->user_id = Auth::guard('api')->user()->id;
                $daily_balance_old->transection_code = $expense->transection_code;
                $daily_balance_old->remark = "Expense of  office";
                $daily_balance_old->save();

            }else{
                $daily_balance_old = DailyBalance::orderBy('id','DESC')->first();
                $daily_balance->balance = $daily_balance_old->balance - $expense->amount;
                $daily_balance->amount = $daily_balance_old->amount - $expense->debit;
                $daily_balance->credit = $daily_balance_old->credit + $expense->credit ;
                $daily_balance->debit = $daily_balance_old->debit + $expense->debit;
                $daily_balance->added_on = date('Y-m-d');
                $daily_balance->remark = "Expense of  office";
                $daily_balance->transection_code = $expense->transection_code;
                $daily_balance->user_id = Auth::guard('api')->user()->id;
                $daily_balance->save();
            }
        }

        return response()->json(['expense'=>$expense],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expense = Expense::with('expense_topic')->findOrFail($id);
        return response()->json(['expense'=>$expense],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
