<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPayment extends Model
{




    protected $fillable = ['project_id','amount','payment_type','date','remark','client_id','user_id','payment_nature','title','doc','cheque_number','bank_name','transection_code'];


    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function client()
    {
        return $this->belongsTo(User::class,'client_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public static function stringSeperator($string)
    {
        $numbers =array();
        $array = str_split($string);
        for($x = 0; $x< count($array); $x++){
            if(is_numeric($array[$x]))
                array_push($numbers,$array[$x]);
        }
        $numbers = implode($numbers);
        return $numbers;
    }
}
