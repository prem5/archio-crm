<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type','status','vat_no','contact_no','contractor_info_id','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function contractor_infos()
    {
        return $this->hasMany(ContractorInfo::class);
    }

    public function user_type()
    {
        return $this->belongsTo(UserType::class,'type');
    }
}
