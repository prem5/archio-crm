<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
    public function daily_stocks(){
        return $this->hasMany(DailyStock::class);
    }
}
