<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectGoods extends Model
{
    use SoftDeletes;

    protected $fillable = ['date','project_id','product_id','quantity','staff_name','transection_code'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function purchase()
    {
        return $this->belongsTo(Purchase::class,'purchase_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
