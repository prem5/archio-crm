<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contractor extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id','contractor_id'];
    public function contractors()
    {
        return $this->belongsTo(User::class,'contractor_id');
    }
    public function project()
    {
       return $this->belongsTo(Project::class);
    }
}
