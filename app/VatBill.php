<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VatBill extends Model
{

    use SoftDeletes;

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function client()
    {
        return $this->belongsTo(User::class,'client_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
