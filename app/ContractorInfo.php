<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractorInfo extends Model
{
    public function contractor_type()
    {
        return $this->belongsTo(ContractorType::class,'contractor_type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
