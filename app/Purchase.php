<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_id','vendor_id','date','name','good_type','quantity','unit','rate','total_price','staff_name','purchased_by'];

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function vendors()
    {
        return $this->belongsTo(User::class,'vendor_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

}
