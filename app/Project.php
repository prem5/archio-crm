<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Project extends Model
{


    protected $fillable = ['name','user_id','client_id','project_manager','start_date','end_date','price','status'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
      return  $this->belongsTo(User::class,'client_id');
    }

    public function project_managers()
    {
        return $this->belongsTo(User::class,'project_manager');
    }

    public function payments()
    {
        return $this->hasMany(ProjectPayment::class);
    }

    public function goods()
    {
        return $this->hasMany(ProjectGoods::class);
    }

    public function transection()
    {
        $transections =[];
        $this->payments();

    }
}
