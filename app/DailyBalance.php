<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyBalance extends Model
{
    public function project_payment()
    {
        return $this->belongsTo(ProjectPayment::class,'transection_code','transection_code');
    }
    public function vendor_payment()
    {
        return $this->belongsTo(VendorPayment::class,'transection_code','transection_code');
    }
    public function contractor_payment()
    {
        return $this->belongsTo(ContractorPayment::class,'transection_code','transection_code');
    }

    public function purchase()
    {
        return $this->belongsTo(Purchase::class,'transection_code','transection_code');
    }

    public function expense()
    {
        return $this->belongsTo(Expense::class,'transection_code','transection_code');
    }

    public function vat_bill()
    {
       return $this->belongsTo(VatBill::class,'transection_code','transection_code');
    }


}
