<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorPayment extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_id','vendor_id','project_id','title','amount','payment_type','remark','date','payment_nature','doc','cheque_number','bank_name','transection_code'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class,'project_id');
    }

    public function vendor()
    {
        return $this->belongsTo(User::class,'vendor_id');
    }
}
