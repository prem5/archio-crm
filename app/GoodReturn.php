<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodReturn extends Model
{
    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function to_project()
    {
        return $this->belongsTo(Project::class,'to_project');

    }
}
