<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix'=>'myadmin','middleware'=>'auth'],function (){
    Route::get('','Admin\\AdminController@index');
    Route::resource('projects','Admin\\ProjectController');
    Route::resource('users','Admin\\UserController');
    Route::resource('payment/clients','Admin\\ProjectPaymentController');
    Route::resource('payment/vendors','Admin\\VendorPaymentController');
    Route::resource('payment/contractors','Admin\\ContractorPaymentController');
    Route::resource('contractors','Admin\\ContractorController');
    Route::resource('purchases','Admin\\PurchaseController');
    Route::resource('good_supplies','Admin\\GoodsController');
});
Route::group(['middleware'=>'auth'],function (){
    Route::get('','Admin\\AdminController@index');
});
Route::get('login','HomeController@getLogin')->name('login');
Route::post('login','HomeController@postLogin');
Route::get('logout','HomeController@getLogout');


