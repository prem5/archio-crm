<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix'=>'v1'],function (){

    Route::group(['prefix'=>'myadmin','middleware'=>'auth:api'],function (){

        Route::get('/user','Admin\\UserController@getProfile');

        Route::get('user_types','Admin\UserController@getUserTypes');
        Route::resource('users','Admin\\UserController');
        Route::get('projects/client_managers','Admin\\ProjectController@get_client_managers');
        Route::get('project_reports','Admin\\ProjectController@getProjects');

        Route::get('project_reports/{id}','Admin\\ProjectController@getProjectReport');
        Route::get('client_reports/{id}','Admin\\ProjectController@getClientReport');
        Route::get('vendor_reports/{id}','Admin\\ProjectController@getVendorReport');
        Route::get('contractor_reports/{id}','Admin\\ProjectController@getContractorReport');
        Route::get('stock_reports/date','Admin\\GoodsController@getStockReport');
        Route::get('get_projects','Admin\\GoodsController@getProjects');
        Route::get('project_goods_report/{id}','Admin\\GoodsController@getGoodReport');
        Route::get('my_products','Admin\\GoodsController@getProducts');
        Route::get('my_products/{id}','Admin\\GoodsController@getProductStock');

        Route::resource('projects','Admin\\ProjectController');
        Route::get('units','Admin\\ProductController@getUnits');
        Route::resource('products','Admin\\ProductController');
        Route::resource('contractors','Admin\\ContractorController');
        Route::resource('payment/clients','Admin\\ProjectPaymentController');
        Route::resource('payment/vendors','Admin\\VendorPaymentController');
        Route::resource('payment/contractors','Admin\\ContractorPaymentController');
        Route::resource('payment/vat_bills','Admin\\VatBillController');
        Route::post('contractor_vat_bills/{id}','Admin\\ContractorVatBillController@update');
        Route::resource('contractor_vat_bills','Admin\\ContractorVatBillController');


        Route::post('reports/date','Admin\\ReportController@date_report');

        Route::get('purchases/vendors','Admin\\PurchaseController@getVendors');
        Route::resource('purchases','Admin\\PurchaseController');
        Route::resource('good_supplies','Admin\\GoodsController');

        Route::get('goods_return/get_goods/{id}','Admin\\GoodsReturnController@getGoods');
        Route::resource('goods_return','Admin\\GoodsReturnController');
        Route::resource('expense_names','Admin\\ExpenseTopicController');
        Route::resource('expenses','Admin\\ExpenseController');
        Route::resource('contractor_types','Admin\\ContractorTypeController');
        Route::resource('units','Admin\\UnitController');

        Route::get('check_user','HomecController@heck_user');

//        Route::resource('products')
    });
    Route::post('login','HomeController@postLogin');
    Route::get('logout','HomeController@getLogout');
});
Route::post('test',function (){
    return "test";
});



